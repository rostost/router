<?php
/**
 * @author      Alex Yakauleu <aliaksyak@gmail.com>
 * @copyright   Copyright (c), 2019 Alex Yakauleu
 * @license     MIT public license
 */

namespace AlexCo\Router;


final class Router{
    const ROUTER_GLOBALS_PREFIX = 'ALEXCO_ROUTER_RESOURCES';

    /**
     * Record Resource in as system
     *
     * @param string $resourceName
     */
    public static function resource(string $resourceName) :void
    {
        //Register Globals for defined path
        self::setGlobalResource($resourceName);
    }

    /**
     * Write Resource into Globals single var.
     *
     * later can be changed to some other storage
     * @param string $resourceName
     */
    public static function setGlobalResource(string $resourceName) :void
    {
        $stored_resources = [];
        if(isset($GLOBALS[self::generateGlobalResourceKey()])){
            $global_value = json_decode($GLOBALS[self::generateGlobalResourceKey()], 1);
            if(!empty($global_value) and is_array( $global_value))
            {
                $stored_resources = $global_value;
            }
        }

        $stored_resources[] = $resourceName;

        $GLOBALS[self::generateGlobalResourceKey()] = json_encode($stored_resources);
    }

    /**
     * Get Resource info from Globals single var.
     *
     * later can be changed to some other storage
     * @param string $resourceName
     */
    public static function getGlobalResources() :array
    {
        $stored_resources = [];
        if(isset($GLOBALS[self::generateGlobalResourceKey()])){
            $global_value = json_decode($GLOBALS[self::generateGlobalResourceKey()], 1);
            if(!empty($global_value) and is_array( $global_value))
            {
                $stored_resources = $global_value;
            }
        }

        return $stored_resources;
    }

    /**
     * Check if globals var was written right
     *
     * @param string $resourceName
     * @return bool
     */
    public static function isResourceStored(string $resourceName) :bool
    {
        if(isset($GLOBALS[self::generateGlobalResourceKey()])){
            $stored_resources = json_decode($GLOBALS[self::generateGlobalResourceKey()], 1);
            if(!is_array($stored_resources)){
                $stored_resources = [];
            }
        }else
            $stored_resources = [];

        return in_array($resourceName, $stored_resources);
    }

    /**
     * Generate Var key for Globals
     *
     * @param string $resourceName
     * @return string
     */
    static function generateGlobalResourceKey() :string
    {
        return self::ROUTER_GLOBALS_PREFIX;
    }

    /**
     * Generate Content from Class and methods
     *
     * @return string
     */
    public static function generateContent(string $namespace)
    {
        $routerDynamic = new RouterDynamic($namespace);
        $status = $routerDynamic->generateResponse();

        if($status){
            $controllerClass = $routerDynamic->getCodeController();

            if(self::isRouteConfigured($controllerClass))
            {
                return $routerDynamic->execute();
            }
        }
    }

    public function resourceNameToController($resourceName) :string
    {
        $class = '';
        $parsed = explode('.', $resourceName);
        foreach($parsed as $classResource)
        {
            $class .= ucfirst($classResource);
        }
        return $class;
    }


    static public function isRouteConfigured($controller) :bool
    {
        $stored_resources = self::getGlobalResources();

        foreach($stored_resources as $stored_resource)
        {
            $className = self::resourceNameToController($stored_resource);

            if($controller === $className){
                return true;
            }
        }
        return false;
    }
}