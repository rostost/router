<?php
/**
 * @author      Alex Yakauleu <aliaksyak@gmail.com>
 * @copyright   Copyright (c), 2019 Alex Yakauleu
 * @license     MIT public license
 */

namespace AlexCo\Router;

use AlexCo\Router\Exception\ExceptionInterface;
use AlexCo\Router\Exception\InvalidArgumentException;
use AlexCo\Router\Exception\InvalidMethodException;

use ReflectionClass;


/**
 * Class RouterDynamic
 * @package AlexCo\Router
 *
 * RouterDynamic is a Class responsible for loading routes from current request and request params
 */
class RouterDynamic{
    function __construct($namespace)
    {
        $this->setNamespace($namespace);
    }


    /**
     * Constant for Pre-defined methods
     *
     * GETINDEX is a custom method needed for difference between indexAction and getAction methods
     */
    private const METHODS = [
        'GET' => [
            'class_method' => 'getAction',
            'argumentNeeded' => true
        ],
        'GETINDEX' => [
            'class_method' => 'indexAction',
            'argumentNeeded' => false
        ],
        'POST' => [
            'class_method' => 'createAction',
            'argument_needed' => true
        ],
        'DELETE' => [
            'class_method' => 'deleteAction',
            'argument_needed' => true
        ],
        'PATCH' => [
            'class_method' => 'updateAction',
            'argument_needed' => true
        ],
    ];

    /**
     * Received HTTP Headers from request
     *
     * @var array
     */
    private $headers = [];

    /**  @var string   */
    private $request_method = '';

    /** @var string  */
    private $namespace = '';

    /** @var  string*/
    private $serverBasePath;

    /**
     *  Execution Controller
     *
     * @var string
     */
    private $codeController = '';

    /**
     * Execution class Method
     *
     * @var string
     */
    private $codeMethod;

    /**
     * Execution class Method arguments
     *
     * @var array
     */
    private $codeMethodArguments = [];


    /*
     *  Generate array with our code methods mappings
     */

    public function generateResponse()
    {
        $this->setRequestHeaders(self::generateHeadersFromRequest());
        $this->setRequestMethod(self::generateMethodFromHeaders($this->getRequestHeaders()));

        $r_uri = $this->getCurrentRequestUri();
        $this->parseUri($r_uri);

        return true;
    }



    /**
     * Main Parsing function, which converts URI to Controller, Method and Args
     *
     * @param string $r_uri
     */
    public function parseUri(string $r_uri)
    {
        $parts = explode('/', $r_uri);

        if(isset($parts[1]) and !isset($parts[2]))
        {
            $this->setController( $parts[1] );

            if($this->getRequestMethod() === 'GET'){
                $this->setRequestMethod('GETINDEX');
            }
        }
        elseif(isset($parts[1]) and !isset($parts[3]))
        {
            $this->setController( $parts[1] );
            $this->setMethodArgs([$parts[2]]);
        }
        elseif(isset($parts[3]) and !isset($parts[4]))
        {
            $this->setController($parts[1]);
            $this->setController($parts[3]);
            $this->setMethodArgs([$parts[2]]);

            if($this->getRequestMethod() === 'GET'){
                $this->setRequestMethod('GETINDEX');
            }
        }
        else
        {
            $this->setController( $parts[1] );
            $this->setController( $parts[3] );
            $this->setMethodArgs([$parts[2], $parts[4]]);
        }

        $this->setCodeMethod($this->getClassMethodFromRequestMethod());

        if($this->getArgumentValidationFromRequestMethod() and empty($this->getMethodArgs())){
            throw InvalidArgumentException::fromCode();
        }
    }


    /**
     * Execution method which try to run methods from parsed args
     *
     * @throws InvalidMethodException
     * @throws \ReflectionException
     */

    public function execute()
    {
        $fn = $this->getCodeMethod();

        if (is_callable($fn)) {
            call_user_func_array($fn, $this->getMethodArgs());
        }
        // If not, check the existence of special parameters
        else{
            $controller = $this->getCodeController() . 'Controller';

            // If namespace has been set Adjust controller class
            if ($this->getNamespace() !== '') {
                $controller = $this->getNamespace() . '\\' . $controller;
            }

            // Check if class exists, if not just ignore and check if the class exists on the default namespace
            if (class_exists($controller)) {
                $reflectionMethod = new \ReflectionMethod($controller, $fn);
                echo $reflectionMethod->invokeArgs(new $controller(), $this->getMethodArgs());
            }else{
                throw InvalidMethodException::fromCode($controller, $fn);
            }
        }
    }


    public function getCodeController() :string
    {
        return $this->codeController;
    }

    public function setController(string  $controller) :void
    {
        $controller = ucfirst($controller);
        if(!empty($this->getCodeController())){
            $this->codeController .= $controller;
        }else{
            $this->codeController = $controller;
        }
    }

    public function getCodeMethod() :string
    {
        return $this->codeMethod;
    }

    public function setCodeMethod(string $method) :void
    {
        $this->codeMethod = $method;
    }

    /*
     * This methods use Our definition constant
     */
    function getClassMethodFromRequestMethod(){
        return self::METHODS[$this->getRequestMethod()]['class_method'];
    }

    function getArgumentValidationFromRequestMethod(){
        return self::METHODS[$this->getRequestMethod()]['argument_needed'];
    }

    public function getMethodArgs() :array
    {
        return $this->codeMethodArguments;
    }

    public function setMethodArgs(array $args) :void
    {
        $this->codeMethodArguments = $args;
    }

    /**
     * Validator for Request Type method
     *
     * @param string $method
     * @return bool
     */
    public function validateMethod(string $method)
    {
        if(!array_key_exists(strtoupper($method), self::METHODS)){
            throw InvalidMethodException::fromCode(get_class(), $method);
        }
    }

    public function setRequestMethod($method = false)
    {
        if(!$method){
            $method = self::generateMethodFromRequest();
        }
        try{
            $this->validateMethod($method);
            $this->request_method = $method;

        }catch (ExceptionInterface $e){
            $this->errors[] = $e->getMessage();
        }catch (\Exception $e){
            $this->errors[] = 'Unexpected Error Happened';
        }
    }

    public function getRequestMethod() :string
    {
        return $this->request_method;
    }

    /**
     * Get all request headers.
     *
     * @return array The request headers
     */
    public function getRequestHeaders() :array
    {
        return $this->headers;
    }

    public function setRequestHeaders(array $headers) :void
    {
        $this->headers = array_merge($this->headers, $headers);
    }

    public static function generateHeadersFromRequest() :array
    {
        $headers = [];
        // If getallheaders() is available, use that
        if (function_exists('getallheaders')) {
            $headers = getallheaders();
            // getallheaders() can return false if something went wrong
            if ($headers !== false) {
                return $headers;
            }
        }
        // Method getallheaders() not available or went wrong: manually extract 'm
        foreach ($_SERVER as $name => $value) {
            if ((substr($name, 0, 5) == 'HTTP_') || ($name == 'CONTENT_TYPE') || ($name == 'CONTENT_LENGTH')) {
                $headers[str_replace([' ', 'Http'], ['-', 'HTTP'], ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }


    /**
     * Set a Default Namespace for methods.
     */
    public function setNamespace(string $namespace)
    {
        $this->namespace = $namespace;
    }

    public function getNamespace() :string
    {
        return $this->namespace;
    }


    /**
     * Define the current relative URI.
     */
    public function getCurrentRequestUri() :string
    {
        // Get the Request URI
        $uri = substr(rawurldecode($_SERVER['REQUEST_URI']), strlen($this->getServerBasePath()));

        // Skip query params into account on the URL
        if (strstr($uri, '?')) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }

        //Leave only one slash in the start
        return '/' . trim($uri, '/');
    }

    /**
     * Return base server Path
     */
    public function getServerBasePath()
    {
        // Check if server base path is defined, if not define it.
        if ($this->serverBasePath === null) {
            $this->serverBasePath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
        }
        return $this->serverBasePath;
    }


    /**
     * Get the request method used, taking overrides into account.
     *
     * @return string The Request method to handle
     */
    public static function generateMethodFromHeaders(array $headers) :string
    {
        // Take the method as found in $_SERVER
        $method = $_SERVER['REQUEST_METHOD'];
        // If it's a HEAD request override it to being GET and prevent any output, as per HTTP Specification
        // @url http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.4
        if ($_SERVER['REQUEST_METHOD'] == 'HEAD') {
            $method = 'GET';
        }
        // If it's a POST request, check for a method override header
        elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $headers = $headers;
            if (isset($headers['X-HTTP-Method-Override']) && in_array($headers['X-HTTP-Method-Override'], ['PUT', 'DELETE', 'PATCH'])) {
                $method = $headers['X-HTTP-Method-Override'];
            }
        }
        return $method;
    }

    /**
     * This is needed to set headers, sessions before output will trigger content
     *
     * //@TODO refactorplan:  ob_start need to be in the init of class  and ob_end_clean in the final step
     */
    static function generateOutput($content)
    {
        ob_start();
        echo $content;
        $ob_content = ob_get_contents();
        ob_end_clean();


        echo $ob_content;
    }


}