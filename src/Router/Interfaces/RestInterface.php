<?php
namespace AlexCo\Router\Interfaces;

interface RestInterface{
    public function indexAction();
    public function getAction(string $id);
    public function createAction();
    public function updateAction(string $id);
    public function deleteAction(string $id);
}