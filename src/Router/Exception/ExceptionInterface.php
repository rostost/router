<?php

namespace AlexCo\Router\Exception;

use Throwable;

/**
 * Base exception marker interface
 */
interface ExceptionInterface extends Throwable
{
}
