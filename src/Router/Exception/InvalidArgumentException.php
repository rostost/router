<?php

namespace AlexCo\Router\Exception;

use InvalidArgumentException as BaseInvalidArgumentException;

/**
 * Exception for invalid http methods provided to the RouterDynamic
 */
class InvalidArgumentException extends BaseInvalidArgumentException implements ExceptionInterface
{
    public static function fromCode(string $className = 'Router') : self
    {
        return new self(sprintf('The provided method requires argument. Class: %s', $className));
    }
}
