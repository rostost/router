<?php

namespace AlexCo\Router\Exception;

use InvalidArgumentException as BaseInvalidArgumentException;

/**
 * Exception for invalid http methods provided to the RouterDynamic
 */
class InvalidMethodException extends BaseInvalidArgumentException implements ExceptionInterface
{
    public static function fromCode(string $className, string $method) : self
    {
        return new self(sprintf('The provided method "%s" "%s" is not supported', $className, $method));
    }
}
