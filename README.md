#AlexCo Router/PHP

[![Source](https://img.shields.io/badge/build-passing-green)](https://bitbucket.org/rostost/router) [![Version](https://img.shields.io/badge/version-1.0.*-blue)](https://bitbucket.org/rostost/router)  [![License](https://img.shields.io/packagist/l/bramus/router.svg?style=flat-square)](https://bitbucket.org/rostost/router/src/master/LICENSE)

Designed to take a url segment and automatically invoke a controller class. 

It does this by breaking up the url segments that come
in a predictable pattern and invoking the correct class and method 
and inserting parameters into the method invoked. We will be following
a REST-ful convention and be using common http methods. 

Class is aware of the http method and the actual url segment

## Installation

The suggested installation method is via [composer](https://getcomposer.org/):

```sh
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://bitbucket.org/rostost/router"
    }
  ],
  "require": {
    "alexco/router": ">=1.0",
  }
}
```


##Example

**router.php**
```php
use AlexCo\Router;

//Register Routes
Route::resource('patients');
Route::resource('patients.metrics');
```

**index.php**
```php
use AlexCo\Router;

require_once __DIR__ . '/routes.php';

echo Router::generateContent("Project1Example\\Controllers");
```

**.htaccess**
```text
DirectoryIndex index.php

# enable apache rewrite engine
RewriteEngine on

# set your rewrite base
# Edit this in your init method too if you script lives in a subfolder
RewriteBase /

# Deliver the folder or file directly if it exists on the server
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d

# Push every request to index.php
RewriteRule ^(.*)$ index.php [QSA]
```



### Tests

Running the tests is simple:

```php
vendor/bin/phpunit
```



## License
This project is licensed under the MIT License - see the LICENSE
file for details