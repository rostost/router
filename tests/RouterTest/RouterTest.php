<?php

namespace AlexCo\RouterTest;

use AlexCo\Router\Router;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase{

    private $test_cases = [
        'cars' => 'CarsController',
        'cars/1' => 'CarsController',
        'cars/1/doors' => 'CarsDoorsController',
        'cars/1/doors/red' => 'CarsDoorsController',
    ];

    /** @var Router */
    private $router;

    /**
     * {@inheritDoc}
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->router = new Router();
    }

    /**
     * Test if resource generated
     */
    public function testGlobalResourceKey()
    {
        $this->assertEquals('ALEXCO_ROUTER_RESOURCES', $this->router::ROUTER_GLOBALS_PREFIX);
    }

    /**
     * Test if Resource name is stored in needed storage
     */
    public function testResourceGlobalStorage(){
        foreach($this->test_cases as $resource_name => $controller_name){
            $this->router::resource($resource_name);
            $this->assertEquals(true, $this->router::isResourceStored($resource_name));
        }

        //Test for invalid Resource
        $this->assertNotEquals(true, $this->router::isResourceStored('123123123dsadklasdlkasldkdsl'));
    }
}