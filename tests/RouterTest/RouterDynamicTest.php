<?php

namespace AlexCo\RouterTest;

use AlexCo\Router\Router;
use AlexCo\Router\RouterDynamic;
use PHPUnit\Framework\TestCase;

class RouterDynamicTest extends TestCase{

    private $test_cases = [
        'cars' => 'CarsController',
        'cars/1' => 'CarsController',
        'cars/1/doors' => 'CarsDoorsController',
        'cars/1/doors/red' => 'CarsDoorsController',
    ];

    /** @var RouterDynamic */
    private $routerDynamic;

    /**
     * {@inheritDoc}
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->routerDynamic = new RouterDynamic('SOME\\NAMESPSACE');
    }

    /**
     * Set Headers manually and be sure we can get them from RouterDynamic
     */
    public function testHeaders()
    {
        $headers = ['HTTP/1.0 404 Not Found', 'Location: /foo.php'];

        $this->routerDynamic->setRequestHeaders($headers);

        $routeDynamicHeaders = $this->routerDynamic->getRequestHeaders();

        $this->assertIsArray($routeDynamicHeaders);
        foreach($headers as $header){
            $this->assertEquals(true, in_array($header, $routeDynamicHeaders), print_r($routeDynamicHeaders,1));
        }
    }

    /**
     * Validate Request Method setters and validators
     */
    public function testRequestMethodSetters()
    {
        $this->routerDynamic->setRequestMethod('POSTbad');
        $this->assertEmpty($this->routerDynamic->getRequestMethod('POSTbad'));

        $this->routerDynamic->setRequestMethod('POST');
        $this->assertEquals('POST', $this->routerDynamic->getRequestMethod('POST'));
    }


}